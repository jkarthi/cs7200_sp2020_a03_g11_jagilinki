# CS7200_SP2020_A03_G11_JAGILINKI



1. What is the best method for choosing the number of max features: Auto, Square Root, or Log2?

Response - 

    Log2 was found to be the best method for choosing the number of max features

2. What is the best value for ccp_alpha (the pruning parameter) from the range of 0-1? How deep is the tree with each alpha value?

Response -
    The best value of ccp_alpha is 0.2
    The tree has a depth of 20



3. Graphically display the best tree found and list the feature importance associated with it. What variables are most important? What is the Root Node?

Response:
    Displayed Graphically and The best tree found is of depth 20 and the root node is,
    Age <= 21, with:
    mse = 274.632
    samples = 824
    value = 35.739
    
    Here is the list of feature importances values associated with it. 
        [0.30805687, 0.09832118, 0.04722475, 0.15051871, 0.02021829, 0.01488907, 0.03303609, 0.32773504]
    
    X1 - Cement
    X8 - Age 
    X4 - Water

    As in the above order, X1 Cement and X8 Age appears to be most important with values of
    And next comes X4 water followed by other substances

    The root node is, X8 Age
    

4. What is the optimal number of estimators, up to 100?

Response- 
    The RandomForestRegression Algorithms took more computational time running. It kept running forever with GridSearchCV. Using RandomSearchCV is another alternative 
    for searching the hyperparameters. And hyperparameter tuning by lowering the max depth improves the performance of the algorithm.


5. Which model performed the best in terms of RMSE?

Response-
- Comparing RMSE values for RandomForestRegression Algorithm and DecisionTreeRegression, the RMSE values for RandomForestRegression were lower
- Decision Tree Regressor(RMSE Values)
Root Mean Squared Error: 8.186522002352808
Root Mean Squared Error: 6.968498413239529
Root Mean Squared Error: 7.154398448783442
Root Mean Squared Error: 6.564144453334319
Root Mean Squared Error: 9.006809277649946

-Random Forest Regressor(RMSE Values)
Root Mean Squared Error: 4.626331605726286
Root Mean Squared Error: 4.858704140689953
Root Mean Squared Error: 4.855613250763933
Root Mean Squared Error: 5.401834993404011
Root Mean Squared Error: 5.056219454337436


The above values indicates that Rndom Forest has lower RMSE values than Decision Tree Regressor for all of the 5 folds.
